FROM fedora:26

RUN  dnf -y update && dnf install -y golang bash wkhtmltopdf xorg-x11-server-Xvfb pdf2svg qpdf&& dnf clean all
COPY ./ /app
ENTRYPOINT ["go", "run", "/app/server.go"]

